# Simple Log System

Version control for a simple cron job to run a bash script with the intent of creating logs in cases of CPU stress

remember to add on crontab -e or check with crontab -l
as in */1 * * * * /bin/bash /home/ec2-user/YOURPROJECT/logscripts/cpumonitor.sh
this will make sure you get an entry in cases of CPU stress (set to above 60% usage) based on the avarage per minute.

The cron job will also run once every minute making sure you get every possible update
Currently the log is being created at /var/log/cpu.log and that particular file is being streamed to CloudWatch on the 'CPU Process Log' stream

A big issue with custom logs and scalability is that they often don't know for how long those logs will be used or how long the project will run. 
With this in mind, this particular file (cpu.log) has also been added to rsyslog's configs (/etc/rsyslog.d) using the following arguments in hopes to keep the logs treated with all the compression and rotation system default to the SO log system.

```
/var/log/cpu.log {
    # rotate log file weekly
    weekly
    # keeps 4 weeks worth of backlog
    rotate 4
    # create a new (empty) log file after rotating the old one
    create
    delaycompress
    compress
}
```

