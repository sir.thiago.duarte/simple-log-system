#!/bin/bash
cpuuse=$(cat /proc/loadavg | awk '{print $1}')
if [ "$cpuusage" > 60 ]; then 
echo "$(ps -eo pcpu,pmem,args sort --sort=pcpu | cut -d" " -f1-5 | tail)" >> /var/log/cpu.log
fi